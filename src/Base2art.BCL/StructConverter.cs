﻿namespace Base2art
{
    using System;
    using System.Globalization;
    
    public static class StructConverter
    {
        [CLSCompliant(false)]
        public static IStruct<TIn> Struct<TIn>(this TIn input)
            where TIn : struct, IConvertible, IFormattable, IComparable
        {
            return new StructWrapper<TIn>(input);
        }

        private class StructWrapper<TBacking> : IStruct<TBacking>
            where TBacking : struct, IConvertible, IFormattable, IComparable
        {
            private readonly TBacking input;

            public StructWrapper(TBacking input)
            {
                this.input = input;
            }
            
            T IStruct.ConvertTo<T>()
            {
                return this.ConvertInternal<T>();
            }

            byte IStruct.ConvertToByte()
            {
                return this.ConvertInternal<byte>();
            }
            
            sbyte IStruct.ConvertToSByte()
            {
                return this.ConvertInternal<sbyte>();
            }
            
            short IStruct.ConvertToInt16()
            {
                return this.ConvertInternal<short>();
            }
            
            ushort IStruct.ConvertToUInt16()
            {
                return this.ConvertInternal<ushort>();
            }
            
            int IStruct.ConvertToInt32()
            {
                return this.ConvertInternal<int>();
            }
            
            uint IStruct.ConvertToUInt32()
            {
                return this.ConvertInternal<uint>();
            }
            
            long IStruct.ConvertToInt64()
            {
                return this.ConvertInternal<long>();
            }
            
            ulong IStruct.ConvertToUInt64()
            {
                return this.ConvertInternal<ulong>();
            }

            T IStruct<TBacking>.ConvertTo<T>()
            {
                return this.ConvertInternal<T>();
            }
            
            object IStruct.ConvertTo(Type destinationType)
            {
                destinationType.ValidateIsNotNull();
                
                return this.ConvertInternal(destinationType);
            }
            
            private TO ConvertInternal<TO>()
            {
                return (TO)this.ConvertInternal(typeof(TO));
            }
            
            private object ConvertInternal(Type destinationType)
            {
                var inType = typeof(TBacking);
                if (inType.IsEnum)
                {
                    inType = Enum.GetUnderlyingType(inType);
                }
                
                IConvertible value = this.input.ToType(inType, CultureInfo.InvariantCulture) as IConvertible;
                
                var outType = destinationType;
                if (!outType.IsEnum)
                {
                    return value.ToType(outType, CultureInfo.InvariantCulture);
                }
                
                outType = Enum.GetUnderlyingType(outType);
                value = value.ToType(outType, CultureInfo.InvariantCulture) as IConvertible;
                return Enum.ToObject(destinationType, value);
            }
        }
    }
}
