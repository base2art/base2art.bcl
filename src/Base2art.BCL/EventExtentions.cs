﻿namespace Base2art
{
    using System;
    using Base2art.ComponentModel;
    
    public static class EventExtensions
    {
        public static void InvokeSafely(
            this EventHandler eventHandle, object sender, EventArgs args)
        {
            if (eventHandle == null)
            {
                return;
            }
            
            eventHandle(sender, args);
        }
        
        public static void InvokeSafely<T>(
            this EventHandler<T> eventHandle, object sender, T args)
            where T : EventArgs
        {
            if (eventHandle == null)
            {
                return;
            }
            
            eventHandle(sender, args);
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "SjY")]
        public static void InvokeSafely<T>(this EventHandler<ItemEventArgs<T>> eventHandle, object sender, T args)
        {
            if (eventHandle == null)
            {
                return;
            }
            
            eventHandle(sender, new ItemEventArgs<T>(args));
        }
    }
}
