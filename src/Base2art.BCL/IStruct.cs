﻿namespace Base2art
{
    using System;
    
    [CLSCompliant(false)]
    public interface IStruct
    {
        T ConvertTo<T>()
            where T : struct, IConvertible, IFormattable, IComparable;

        byte ConvertToByte();
        
        sbyte ConvertToSByte();
        
        short ConvertToInt16();
        
        ushort ConvertToUInt16();
        
        int ConvertToInt32();
        
        uint ConvertToUInt32();
        
        long ConvertToInt64();
        
        ulong ConvertToUInt64();
        
        object ConvertTo(Type destinationType);
    }
    
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1040:AvoidEmptyInterfaces", Justification = "SjY")]
    public interface IStruct<TBacking> : IStruct
        where TBacking : struct, IConvertible, IFormattable, IComparable
    {
        new T ConvertTo<T>();
    }
}
