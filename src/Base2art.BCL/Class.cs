﻿namespace Base2art
{
    using System;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
    public static class Class
    {
        public static IClass GetClass(this Type type)
        {
            var klazz = typeof(ClassImpl<>);
            var gklazz = klazz.MakeGenericType(type);
            return (IClass)Activator.CreateInstance(gklazz);
        }

        public static IClass<T> GetClass<T>(this T item)
        {
            if (item == null)
            {
                return GetClass<T>();
            }

            return new ClassImpl<T>();
        }

        public static IClass<T> GetClass<T>()
        {
            return new ClassImpl<T>();
        }

        private class ClassImpl<T> : IClass<T>
        {
            public ClassImpl()
            {
            }

            public Type Type
            {
                get
                {
                    return typeof(T);
                }
            }

            IClass<T1> IClass.As<T1>()
            {
                return (IClass<T1>)this;
            }
        }
    }
}
