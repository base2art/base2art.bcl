﻿namespace Base2art
{
    using System;

    public class OneTryLazy<T> : ILazy<T>
    {
        private readonly Func<T> action;

        private readonly object padLock = new object();

        private T value;

        private bool hasValue;

        private Exception exception;

        public OneTryLazy(Func<T> action)
        {
            this.action = action;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public T Value
        {
            get
            {
                if (!this.hasValue)
                {
                    lock (this.padLock)
                    {
                        if (!this.hasValue)
                        {
                            try
                            {
                                this.value = this.action();
                            }
                            catch (Exception ex)
                            {
                                this.exception = ex;
                            }
                            finally
                            {
                                this.hasValue = true;
                            }
                        }
                    }
                }

                if (this.exception != null)
                {
                    throw this.exception;
                }

                return this.value;
            }
        }

        public bool IsValueCreated
        {
            get
            {
                return this.hasValue;
            }
        }

        public void Reset()
        {
            this.exception = null;
            this.hasValue = false;
        }
    }
}