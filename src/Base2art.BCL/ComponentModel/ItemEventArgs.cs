﻿namespace Base2art.ComponentModel
{
    using System;
    
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix", Justification = "SjY")]
    public static class ItemEventArgs
    {
        public static ItemEventArgs<T> Create<T>(T itemValue)
        {
            return new ItemEventArgs<T>(itemValue);
        }
    }
    
    public class ItemEventArgs<T> : EventArgs
    {
        private readonly T obj;

        public ItemEventArgs(T obj)
        {
            this.obj = obj;
        }

        public T Item
        {
            get
            {
                return this.obj;
            }
        }
    }
}
