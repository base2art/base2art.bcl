﻿namespace Base2art.ComponentModel
{
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;
    
    public class UsingAssignmentWrapper<TOut>
    {
        private readonly TOut newValue;

        public UsingAssignmentWrapper(TOut newValue)
        {
            this.newValue = newValue;
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "SjY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
        public IDisposable TransactionallyTo(Expression<Func<TOut>> expr)
        {
            expr.ValidateIsNotNull();
            var newValueParameter = Expression.Parameter(expr.Body.Type);
            var assignExpr = Expression.Assign(expr.Body, newValueParameter);
            var assign = Expression.Lambda<Action<TOut>>(assignExpr, newValueParameter);

            var getter = expr.Compile();
            var setter = assign.Compile();
            
            var oldValue = getter();
            setter(this.newValue);
            return new Wrapper(oldValue, setter);
        }
        
        private class Wrapper : Component
        {
            private readonly Action<TOut> setter;

            private readonly TOut oldValue;
            
            public Wrapper(TOut oldValue, Action<TOut> setter)
            {
                this.oldValue = oldValue;
                this.setter = setter;
            }
            
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                if (disposing)
                {
                    this.setter(this.oldValue);
                }
            }
        }
    }
}