﻿namespace Base2art.ComponentModel
{
    using System;
    using System.Linq.Expressions;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
    public static class Using
    {
        public static UsingAssignmentWrapper<TOut> Assign<TOut>(this TOut newValue)
        {
            return new UsingAssignmentWrapper<TOut>(newValue);
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "SjY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
        public static IDisposable TransactionallyAssignTo<TOut>(TOut newValue, Expression<Func<TOut>> expression)
        {
            return newValue.Assign().TransactionallyTo(expression);
        }
    }
}