﻿namespace Base2art
{
    using System;

    [Serializable]
    public class FunctionalCreator<TInput, TResult> : ICreator<TInput, TResult>
    {
        [NonSerialized]
        private readonly Func<TInput, TResult> value;

        public FunctionalCreator(Func<TInput, TResult> value)
        {
            this.value = value;
        }

        public TResult Create(TInput parameter)
        {
            return this.value(parameter);
        }
    }

    [Serializable]
    public class FunctionalCreator<TResult> : ICreator<TResult>
    {
        [NonSerialized]
        private readonly Func<TResult> value;

        public FunctionalCreator(Func<TResult> value)
        {
            this.value = value;
        }

        public TResult Create()
        {
            return this.value();
        }
    }
}
