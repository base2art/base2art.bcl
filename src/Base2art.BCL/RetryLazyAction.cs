﻿namespace Base2art
{
    using System;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public class RetryEnsurer : IEnsurer
    {
        private readonly Action action;

        private readonly object padLock = new object();

        private bool hasValue;

        public RetryEnsurer(Action action)
        {
            this.action = action;
        }

        public bool IsValueCreated
        {
            get { return this.hasValue; }
        }

        public void Ensure()
        {
            if (!this.hasValue)
            {
                lock (this.padLock)
                {
                    if (!this.hasValue)
                    {
                        this.action();
                        this.hasValue = true;
                    }
                }
            }
        }

        public void Reset()
        {
            this.hasValue = false;
        }
    }
}
