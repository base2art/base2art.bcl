﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class ByteParser : ValueParserBase<byte>
    {
        public override byte? ParseOrNull(string value)
        {
            byte parseValue;
            if (byte.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}