﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class StringParser : ObjectParserBase<string>
    {
        public override string ParseOrNull(string value)
        {
            return value;
        }
    }
}