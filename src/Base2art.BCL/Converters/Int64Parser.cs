﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class Int64Parser : ValueParserBase<long>
    {
        public override long? ParseOrNull(string value)
        {
            long parseValue;
            if (long.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}