﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    [CLSCompliant(false)]
    public class SByteParser : ValueParserBase<sbyte>
    {
        public override sbyte? ParseOrNull(string value)
        {
            sbyte parseValue;
            if (sbyte.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}