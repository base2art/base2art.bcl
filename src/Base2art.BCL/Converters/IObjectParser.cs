﻿namespace Base2art.Converters
{
    public interface IObjectParser<T> : IObjectParserBase<T>
    {
        T ParseOrDefault(string value, T defaultValue);

        T ParseOrDefault(string value, ICreator<T> defaultValueProvider);
    }
}