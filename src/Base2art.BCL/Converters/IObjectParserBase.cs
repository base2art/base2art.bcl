﻿namespace Base2art.Converters
{
    public interface IObjectParserBase
    {
        object ParseOrDefault(string value);
    }

    public interface IObjectParserBase<out T> : IObjectParserBase
    {
        new T ParseOrDefault(string value);
    }
}