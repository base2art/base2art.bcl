﻿namespace Base2art.Converters
{
    using System;

    public interface IValueParser<T> : IObjectParser<T>
        where T : struct
    {
        T? ParseOrNull(string value);
    }
}
