﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public abstract class ValueParserBase<T> : IValueParser<T>
        where T : struct
    {
        public abstract T? ParseOrNull(string value);

        public T ParseOrDefault(string value)
        {
            var parsed = this.ParseOrNull(value);
            if (parsed.HasValue)
            {
                return parsed.Value;
            }

            return default(T);
        }

        public T ParseOrDefault(string value, T defaultValue)
        {
            var parsed = this.ParseOrNull(value);
            if (parsed.HasValue)
            {
                return parsed.Value;
            }

            return defaultValue;
        }

        public T ParseOrDefault(string value, ICreator<T> defaultValueProvider)
        {
            var parsed = this.ParseOrNull(value);
            if (parsed.HasValue)
            {
                return parsed.Value;
            }

            if (defaultValueProvider == null)
            {
                return default(T);
            }
            
            return defaultValueProvider.Create();
        }

        object IObjectParserBase.ParseOrDefault(string value)
        {
            return this.ParseOrDefault(value);
        }
    }
}