namespace Base2art.Converters
{
    using System;

    [Serializable]
    public abstract class ObjectParserBase<T> : IObjectParser<T>
        where T : class
    {
        public abstract T ParseOrNull(string value);

        public T ParseOrDefault(string value)
        {
            return this.ParseOrNull(value);
        }

        public T ParseOrDefault(string value, T defaultValue)
        {
            return this.ParseOrDefault(value, new FunctionalCreator<T>(() => defaultValue));
        }

        public T ParseOrDefault(string value, ICreator<T> defaultValueProvider)
        {
            var parsed = this.ParseOrNull(value);
            if (parsed != null)
            {
                return parsed;
            }

            if (defaultValueProvider == null)
            {
                return default(T);
            }
            
            return defaultValueProvider.Create();
        }

        object IObjectParserBase.ParseOrDefault(string value)
        {
            return this.ParseOrDefault(value);
        }
    }
}