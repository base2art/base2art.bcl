﻿namespace Base2art.Converters
{
    using System;
    using System.Net.Mail;

    [Serializable]
    public class MailAddressParser : ObjectParserBase<MailAddress>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public override MailAddress ParseOrNull(string value)
        {
            try
            {
                return new MailAddress(value);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}