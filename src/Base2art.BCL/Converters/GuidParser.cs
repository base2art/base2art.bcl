﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class GuidParser : ValueParserBase<Guid>
    {
        public override Guid? ParseOrNull(string value)
        {
            Guid parseValue;
            if (Guid.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}