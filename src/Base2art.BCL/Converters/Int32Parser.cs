﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class Int32Parser : ValueParserBase<int>
    {
        public override int? ParseOrNull(string value)
        {
            int parseValue;
            if (int.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}