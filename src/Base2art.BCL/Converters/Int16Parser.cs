﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class Int16Parser : ValueParserBase<short>
    {
        public override short? ParseOrNull(string value)
        {
            short parseValue;
            if (short.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}