﻿namespace Base2art.Converters
{
    using System;

    [Serializable]
    public class BooleanParser : ValueParserBase<bool>
    {
        public override bool? ParseOrNull(string value)
        {
            bool parseValue;
            if (bool.TryParse(value, out parseValue))
            {
                return parseValue;
            }

            return null;
        }
    }
}