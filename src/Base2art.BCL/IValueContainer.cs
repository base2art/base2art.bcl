﻿namespace Base2art
{
    public interface IValueContainer<T> : IValueContainerOut<T>
    {
        new T Value { get; set; }
    }
}
