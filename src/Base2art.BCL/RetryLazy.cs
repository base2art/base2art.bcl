namespace Base2art
{
    using System;

    public class RetryLazy<T> : ILazy<T>
    {
        private readonly Func<T> action;

        private readonly object padLock = new object();

        private T value;

        private bool hasValue;

        public RetryLazy(Func<T> action)
        {
            this.action = action;
        }

        public T Value
        {
            get
            {
                if (!this.hasValue)
                {
                    lock (this.padLock)
                    {
                        if (!this.hasValue)
                        {
                            this.value = this.action();
                            this.hasValue = true;
                        }
                    }
                }

                return this.value;
            }
        }

        public bool IsValueCreated
        {
            get
            {
                return this.hasValue;
            }
        }

        public void Reset()
        {
            this.hasValue = false;
        }
    }
}