﻿namespace Base2art
{
    public interface IValueContainerOut<out T>
    {
        T Value
        {
            get;
        }
    }
}
