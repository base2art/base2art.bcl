﻿namespace Base2art.Text
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;
    
    /// <summary>
    /// http://stackoverflow.com/questions/3708454/is-there-a-textwriter-child-class-that-fires-event-if-text-is-written
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public class EventedStringWriter : StringWriter
    {
        private readonly bool autoFlush;
        
        private readonly StringBuilder builder;
        
        private int position = 0;
        
        public EventedStringWriter() : this(new StringBuilder(), CultureInfo.InvariantCulture, true)
        {
        }

        public EventedStringWriter(bool autoFlush) : this(new StringBuilder(), CultureInfo.InvariantCulture, autoFlush)
        {
        }

        public EventedStringWriter(StringBuilder builder, CultureInfo cultureInfo, bool autoFlush) : base(builder, cultureInfo)
        {
            this.autoFlush = autoFlush;
            this.builder = builder;
        }
        
        public event EventHandler<StringEventArgs> Flushed;
        
        public event EventHandler<StringEventArgs> Cleared;
        
        public virtual bool AutoFlush
        {
            get { return this.autoFlush; }
        }
        
        public void Clear()
        {
            var sb = this.builder;
            sb.Remove(0, sb.Length);
            this.position = 0;
            this.Cleared.InvokeSafely(this, new StringEventArgs(string.Empty));
        }

        public override void Flush()
        {
            base.Flush();
            var oldPosition = this.position;
            var stringBuilder = this.GetStringBuilder();
            var newPosition = stringBuilder.Length;
            this.position = newPosition;
            string text = stringBuilder.ToString(oldPosition, newPosition - oldPosition);
            
            this.OnFlush(text);
        }

        public override void Write(char value)
        {
            base.Write(value);
            this.TryAutoFlush();
        }

        public override void Write(string value)
        {
            base.Write(value);
            this.TryAutoFlush();
        }

        public override void Write(char[] buffer, int index, int count)
        {
            base.Write(buffer, index, count);
            this.TryAutoFlush();
        }

        protected void OnFlush(string value)
        {
            this.Flushed.InvokeSafely(this, new StringEventArgs(value));
        }

        private void TryAutoFlush()
        {
            if (this.AutoFlush)
            {
                this.Flush();
            }
        }
    }
}
