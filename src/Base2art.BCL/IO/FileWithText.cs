﻿namespace Base2art.IO
{
    using System;
    using System.ComponentModel;
    using System.IO;

    public class FileWithText : Component
    {
        private readonly string path;

        private readonly string text;
        
        public FileWithText(string path, string text)
        {
            this.text = text;
            this.path = path;
            File.WriteAllText(this.path, this.text);
        }

        public void DeleteFile(string path, int retries)
        {
            this.DeleteFile(path, retries, 0);
        }

        public void DeleteFile(string path, int retries, int currentCount)
        {
            if (currentCount > retries)
            {
                return;
            }
            
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(this.path);
                }
            }
            catch (IOException)
            {
                this.DeleteFile(path, retries, currentCount + 1);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.DeleteFile(this.path, 3);
            }
        }
    }
}
