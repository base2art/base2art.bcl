﻿namespace Base2art.IO
{
    using System;
    using System.IO;
    using System.Linq;

    public static class FileSystemExtender
    {
        public static DirectoryInfo Parent(this DirectoryInfo info)
        {
            info.ValidateIsNotNull();
            
            return info.Parent;
        }

        public static DirectoryInfo Dir(this DirectoryInfo info, string name)
        {
            info.ValidateIsNotNull();
            name.ValidateIsNotNullOrWhitespace();
            
            return info.EnumerateDirectories()
                .FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public static FileInfo File(this DirectoryInfo info, string name)
        {
            info.ValidateIsNotNull();
            name.ValidateIsNotNullOrWhitespace();
            
            return info.EnumerateFiles()
                .FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }
    }
}
