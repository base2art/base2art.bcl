﻿namespace Base2art.Text
{
    using System;
    
    public class StringEventArgs : EventArgs
    {
        private readonly string value;

        public StringEventArgs(string value)
        {
            this.value = value;
        }

        public string CurrentChunk
        {
            get
            {
                return this.value;
            }
        }
    }
}
