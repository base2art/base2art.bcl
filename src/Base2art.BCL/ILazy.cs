﻿namespace Base2art
{
    public interface ILazy<out T> : IValueContainerOut<T>
    {
        bool IsValueCreated { get; }

        void Reset();
    }
}