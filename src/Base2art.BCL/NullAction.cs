﻿namespace Base2art
{
    using System;

    [Serializable]
    public class NullAction<T1, T2> : IAction<T1, T2>
    {
        public void Invoke(T1 item1, T2 item2)
        {
        }
    }

    [Serializable]
    public class NullAction<T> : IAction<T>
    {
        public void Invoke(T item)
        {
        }
    }
}