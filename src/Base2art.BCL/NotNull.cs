﻿namespace Base2art
{
    using System;
    
    public struct NotNull<T> : IValueContainerOut<T>, IEquatable<NotNull<T>>
        where T : class, new()
    {
        private readonly T value;
        
        public NotNull(T value)
        {
            this.value = value;
        }

        public T Value
        {
            get
            {
                if (this.value == null)
                {
                    return new T();
                }
                
                return this.value;
            }
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates", Justification = "SjY")]
        public static implicit operator T(NotNull<T> currentValue)
        {
            return currentValue.Value;
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates", Justification = "SjY")]
        public static implicit operator NotNull<T>(T currentValue)
        {
            return new NotNull<T>(currentValue);
        }
        
        public static bool operator ==(NotNull<T> left, NotNull<T> right)
        {
            return left.Equals(right);
        }
        
        public static bool operator !=(NotNull<T> left, NotNull<T> right)
        {
            return !left.Equals(right);
        }
        
        public override bool Equals(object obj)
        {
            return obj is NotNull<T> && this.Equals((NotNull<T>)obj);
        }
        
        public bool Equals(NotNull<T> other)
        {
            return this.value == other.value;
        }
        
        public override int GetHashCode()
        {
            return this.value == null ? 0 : this.value.GetHashCode();
        }
    }
    
    public static class NotNull
    {
        public static NotNull<T> Create<T>(T value)
            where T : class, new()
        {
            return new NotNull<T>(value);
        }
    }
}
