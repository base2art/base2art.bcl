﻿namespace Base2art
{
    public interface ICreator<in TInput, out TResult>
    {
        TResult Create(TInput parameter);
    }

    public interface ICreator<out TResult>
    {
        TResult Create();
    }
}