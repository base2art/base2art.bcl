﻿namespace Base2art
{
    using System;

    [Serializable]
    public class NullCreator<TInput, TResult>
        : ICreator<TInput, TResult>
    {
        private readonly TResult value;

        public NullCreator()
        {
            this.value = default(TResult);
        }

        public NullCreator(TResult value)
        {
            this.value = value;
        }

        public TResult Create(TInput parameter)
        {
            return this.value;
        }
    }

    [Serializable]
    public class NullCreator<TResult> : ICreator<TResult>
    {
        private readonly TResult value;

        public NullCreator()
        {
            this.value = default(TResult);
        }

        public NullCreator(TResult value)
        {
            this.value = value;
        }

        public TResult Create()
        {
            return this.value;
        }
    }
}