﻿namespace Base2art
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public interface IEnsurer
    {
        bool IsValueCreated
        {
            get;
        }
        
        void Ensure();

        void Reset();
    }
}
