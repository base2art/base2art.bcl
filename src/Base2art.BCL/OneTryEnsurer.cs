﻿namespace Base2art
{
    using System;
    
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public class OneTryEnsurer : IEnsurer
    {
        private readonly Action action;

        private readonly object padLock = new object();

        private bool hasValue;

        private Exception exception;

        public OneTryEnsurer(Action action)
        {
            this.action = action;
        }

        public bool IsValueCreated
        {
            get
            {
                return this.hasValue;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public void Ensure()
        {
            if (!this.hasValue)
            {
                lock (this.padLock)
                {
                    if (!this.hasValue)
                    {
                        try
                        {
                            this.action();
                        }
                        catch (Exception ex)
                        {
                            this.exception = ex;
                        }
                        finally
                        {
                            this.hasValue = true;
                        }
                    }
                }
            }
            
            if (this.exception != null)
            {
                throw this.exception;
            }
        }

        public void Reset()
        {
            this.exception = null;
            this.hasValue = false;
        }
    }
}
