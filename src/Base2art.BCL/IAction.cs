namespace Base2art
{
    public interface IAction<in T1, in T2>
    {
        void Invoke(T1 item1, T2 item2);
    }

    public interface IAction<in T>
    {
        void Invoke(T item);
    }
}