﻿namespace Base2art.Net
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
    using Base2art.Collections.Generic;
	using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class SecureCookieJarFeature
    {
        [Test]
        public void ShouldHaveApi()
        {
            var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440"});
            MultiMap<string, string> inMap = new MultiMap<string, string>();
            inMap.Add("name", "Scott Youngblut");
            inMap.Add("Age", "1984-10-13");
            cookieJar.SetSecureCookieValues("user-account", inMap);
            cookieJar.GetCookieValue("user-account").Should().Be("/scs/783B77AE71C90F5C3150CEA127487685?name=Scott%20Youngblut&Age=1984-10-13");
            
            var map = cookieJar.GetSecureCookieValues("user-account");
            map["name"].First().Should().Be("Scott Youngblut");
            map["Age"].First().Should().Be("1984-10-13");
            
            cookieJar.DeleteSecureCookie("user-account");
            cookieJar.GetSecureCookieValues("user-account").Count.Should().Be(0);
        }
        
        [Test]
        public void ShouldGetMapFromString()
        {
            var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440"});
            cookieJar.SetCookieValue("user-account", "/scs/783B77AE71C90F5C3150CEA127487685?name=Scott%20Youngblut&Age=1984-10-13");
            
            var map = cookieJar.GetSecureCookieValues("user-account");
            map["name"].First().Should().Be("Scott Youngblut");
            map["Age"].First().Should().Be("1984-10-13");
            
            cookieJar.DeleteSecureCookie("user-account");
            cookieJar.GetSecureCookieValues("user-account").Count.Should().Be(0);
        }
        
        [Test]
        public void ShouldGetMapFromStringUserCorrupted()
        {
            var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440"});
            cookieJar.SetCookieValue("user-account", "/783B77AE71C90F5C3150CEA127487685?name=Scott%20youngblut&Age=1984-10-13");
            cookieJar.GetSecureCookieValues("user-account").Count.Should().Be(0);
        }
        
        
        [Test]
        public void ShouldGetEmptyCollectionWhenNoCookieExists()
        {
            var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440"});
            cookieJar.GetSecureCookieValues("user-account").Count.Should().Be(0);
        }
        
        [Test]
        public void ShouldLoad()
        {
            Dictionary<string, string> cookies = new Dictionary<string, string>();
            var cookieJar = new KeyValuePairCookieJar(cookies, new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440", SecureCookiePrefix = "scs"});
            var map = new MultiMap<string, string>();
            map.Add("f_name", "scott");
            map.Add("l_name", "Youngblut");
            cookieJar.SetSecureCookieValues("person", map);
            cookieJar.GetCookieValue("person").Should().Be("/scs/D398A459552486CD9044D5AD508BE37C?f_name=scott&l_name=Youngblut");
            cookies["person"].Should().Be("/scs/D398A459552486CD9044D5AD508BE37C?f_name=scott&l_name=Youngblut");
        }
        
        [Test]
        public void ShouldDeleteCookieIfCollectionIsEmpty()
        {
            Dictionary<string, string> cookies = new Dictionary<string, string>();
            var cookieJar = new KeyValuePairCookieJar(cookies, new SecureCookieJarSettings{ApplicationSaltSettings = "AC431625-C529-4238-A1BF-4B1308E6E440"});
            var map = new MultiMap<string, string>();
            map.Add("f_name", "scott");
            map.Add("l_name", "Youngblut");
            cookieJar.SetSecureCookieValues("person", map);
            cookieJar.GetCookieValue("person").Should().Be("/scs/D398A459552486CD9044D5AD508BE37C?f_name=scott&l_name=Youngblut");
            cookies["person"].Should().Be("/scs/D398A459552486CD9044D5AD508BE37C?f_name=scott&l_name=Youngblut");
            
            cookieJar.SetSecureCookieValues("person", new MultiMap<string, string>());
            cookies.ContainsKey("person").Should().BeFalse();
        }
    }
}
