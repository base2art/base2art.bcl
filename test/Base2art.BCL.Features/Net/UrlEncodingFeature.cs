﻿namespace Base2art.Net
{
	using System;
	using System.Linq;
	using Base2art.Collections.Generic;
    using Base2art.Net;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class UrlEncodingFeature
    {
        [Test]
        public void ShouldParseQueryStringMultiMap()
        {
            var dict = UrlEncodingExtender.ParseValuesAllowingDuplicates("f_name=scott&l_name=Youngblut");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Count().Should().Be(1);
            dict["f_name"].First().Should().Be("scott");
            dict["l_name"].Count().Should().Be(1);
            dict["l_name"].First().Should().Be("Youngblut");
        }
        
        [Test]
        public void ShouldParseQueryStringMultiMapTwoValues()
        {
            var dict = UrlEncodingExtender.ParseValuesAllowingDuplicates("f_name=scott&l_name=Youngblut&f_name=Joseph");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Count().Should().Be(2);
            dict["f_name"].First().Should().Be("scott");
            dict["f_name"].Skip(1).First().Should().Be("Joseph");
            dict["l_name"].Count().Should().Be(1);
            dict["l_name"].First().Should().Be("Youngblut");
        }
        
        [Test]
        public void ShouldParseQueryStringMultiMapMissingValue()
        {
            var dict = UrlEncodingExtender.ParseValuesAllowingDuplicates("f_name=scott&l_name=");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Count().Should().Be(1);
            dict["f_name"].First().Should().Be("scott");
            dict["l_name"].Count().Should().Be(1);
            dict["l_name"].First().Should().Be(string.Empty);
        }
        
        [Test]
        public void ShouldParseQueryStringMultiMapMissingKey()
        {
            var dict = UrlEncodingExtender.ParseValuesAllowingDuplicates("f_name=scott%3D&l_name=&&&&&&");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Count().Should().Be(1);
            dict["f_name"].First().Should().Be("scott=");
            dict["l_name"].Count().Should().Be(1);
            dict["l_name"].First().Should().Be(string.Empty);
        }
        
        [Test]
        public void ShouldParseQueryStringMap()
        {
            var dict = UrlEncodingExtender.ParseValuesNoDuplicates("f_name=scott&l_name=Youngblut");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Should().Be("scott");
            dict["l_name"].Should().Be("Youngblut");
        }
        
        [Test]
        public void ShouldParseQueryStringMapTwoValues()
        {
            var dict = UrlEncodingExtender.ParseValuesNoDuplicates("f_name=scott&l_name=Youngblut&f_name=Joseph");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
//            dict["f_name"].Should().Be("scott");
            dict["f_name"].Should().Be("Joseph");
            dict["l_name"].Should().Be("Youngblut");
        }
        
        [Test]
        public void ShouldParseQueryStringMapMissingValue()
        {
            var dict = UrlEncodingExtender.ParseValuesNoDuplicates("f_name=scott&l_name=");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Should().Be("scott");
            dict["l_name"].Should().Be(string.Empty);
        }
        
        [Test]
        public void ShouldParseQueryStringMapMissingKey()
        {
            var dict = UrlEncodingExtender.ParseValuesNoDuplicates("f_name=scott%3D&l_name=&&&&&&");
            dict.Keys.Should().BeEquivalentTo(new [] {"f_name", "l_name"});
            dict["f_name"].Should().Be("scott=");
            dict["l_name"].Should().Be(string.Empty);
        }
        
        [Test]
        public void ShouldWriteTheMapToQueryString()
        {
            var map = new Map<string, string>();
            map["f_name"] = "Matt";
            map["f_name"] = "Scott";
            map["l_name"] = "Youngblut";
            map["other"] = string.Empty;
            UrlEncodingExtender.Write(map).Should().Be("f_name=Scott&l_name=Youngblut&other=");
        }
        
        [Test]
        public void ShouldWriteTheMultiMapToQueryString()
        {
            var map = new MultiMap<string, string>();
            map.Add("f_name", "Matt");
            map.Add("f_name", "Scott");
            map.Add("l_name", "Youngblut");
            map.Add("other", string.Empty);
            UrlEncodingExtender.Write(map).Should().Be("f_name=Matt&f_name=Scott&l_name=Youngblut&other=");
        }
    }
}
