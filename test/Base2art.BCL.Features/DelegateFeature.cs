﻿namespace Base2art
{
    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class DelegateFeature
    {
        [Test]
        public void ShouldExecuteSideEffectFree()
        {
            var nd = new NullAction<string>();
            nd.Invoke("s");
            var nd1 = new NullAction<string, string>();
            nd1.Invoke("s", "s");

            var fc = new FunctionalCreator<string>(() => "fc");
            fc.Create().Should().Be("fc");
            var fc1 = new FunctionalCreator<string>(() => "fc1");
            fc1.Create().Should().Be("fc1");

            var fcp = new FunctionalCreator<string, string>(x => "fcp");
            fcp.Create("").Should().Be("fcp");
            var fcp1 = new FunctionalCreator<string, string>(x => "fcp1");
            fcp1.Create("").Should().Be("fcp1");

            var nc = new NullCreator<string>();
            nc.Create().Should().Be(null);
            var nc1 = new NullCreator<string, string>();
            nc1.Create("").Should().Be(null);

            var ncp = new NullCreator<string>("abc");
            ncp.Create().Should().Be("abc");
            var ncp1 = new NullCreator<string, string>("abcd");
            ncp1.Create("").Should().Be("abcd");
        }
    }
}
