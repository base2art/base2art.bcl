﻿namespace Base2art
{
	using System;
	using System.Diagnostics;
	using FluentAssertions;
	using NUnit.Framework;

	[TestFixture]
	public class RetryLazyActionFeature
	{
		[Test]
		public void ShouldRetryTheLazy()
		{
			int i = -1;
			IEnsurer lazy = new RetryEnsurer(() => 
			{
				i++;
				if (i < 3)
				{
					throw new InvalidOperationException("Test");
				}
			});
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			lazy.IsValueCreated.Should().BeFalse();
			i.Should().Be(2);
			lazy.Ensure();
			lazy.IsValueCreated.Should().BeTrue();
			lazy.Ensure();
			i.Should().Be(3);
			lazy.IsValueCreated.Should().BeTrue();
			i = -1;
			lazy.Reset();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
			lazy.IsValueCreated.Should().BeFalse();
			i.Should().Be(2);
			lazy.Ensure();
			lazy.IsValueCreated.Should().BeTrue();
			i.Should().Be(3);
			lazy.Ensure();
			i.Should().Be(3);
			lazy.IsValueCreated.Should().BeTrue();
		}
	}
}


