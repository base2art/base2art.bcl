﻿namespace Base2art.Fixtures
{
	using System;

	public class PersonData
	{
		public string Name
		{
			get;
			set;
		}

		public DateTime Birthday
		{
			get;
			set;
		}

		public SchoolLevelData HighestGradeLevel
		{
			get;
			set;
		}

		public string[] Aliases
		{
			get;
			set;
		}
	}
}
