﻿namespace Base2art.Fixtures
{
	using System;

	public class Person : IPerson
	{
		public string Name
		{
			get;
			set;
		}
		
		public string[] Aliases
		{
			get;
			set;
		}

		public SchoolLevel HighestGradeLevel
		{
			get;
			set;
		}

		public DateTime Birthday
		{
			get;
			set;
		}
	}
}



