﻿
namespace Base2art.Fixtures
{
    public class ManagerData
    {
        public EmployeeData EmployeeInformation { get; set; }
        
        public EmployeeData PersonalInformation { get; set; }
		
        public string[] TeamNames { get; set ; }

		public EmployeeData[] DirectReports { get; set ; }
        
        public EmployeeData[] FrendsAtWork { get; set ; }
        
        public PersonData[] Children { get; set ; }
    }
}
