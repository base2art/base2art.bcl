﻿namespace Base2art.Fixtures
{
    public enum SchoolLevel
    {
        MiddleSchool,
        HighSchool,
        College,
        PHD
    }
}
