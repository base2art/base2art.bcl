﻿namespace Base2art
{
    using System;
    using System.Diagnostics;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class RetryLazyFeature
    {
        [Test]
        public void ShouldRetryTheLazy()
        {
            int i = -1;
            ILazy<string> lazy = new RetryLazy<string>(
                () =>
                {
                    i++;
                    if (i < 3)
                    {
                        throw new InvalidOperationException("Test");
                    }

                    return i.ToString();
                });


            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeFalse();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();

            i = -1;
            lazy.Reset();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeFalse();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
        }
    }

    
}
