﻿namespace Base2art.ComponentModel
{
    using System.IO;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class UsingFileFeature
    {
        [Test]
        public void ShouldCreateAndDestroyFile()
        {
            var path = Path.Combine(Path.GetTempPath(), "MyTestFile.tmp");
            File.Exists(path).Should().BeFalse();
            using (new FileWithText(path, "Text"))
            {
                File.Exists(path).Should().BeTrue();
                File.ReadAllText(path).Should().Be("Text");
            }
            
            File.Exists(path).Should().BeFalse();
        }
        
        
        [Test]
        public void ShouldCreateAndBeUnableToDestroyFile()
        {
            var path = Path.Combine(Path.GetTempPath(), "MyTestFileOpenHandle.tmp");
            File.Exists(path).Should().BeFalse();
            FileStream fs = null;
            using (new FileWithText(path, "Text"))
            {
                File.Exists(path).Should().BeTrue();
                File.ReadAllText(path).Should().Be("Text");
                fs = File.Open(path,FileMode.Open);
            }
            
            File.Exists(path).Should().BeTrue();
            fs.Close();
            File.Delete(path);
            File.Exists(path).Should().BeFalse();
        }
    }
}
