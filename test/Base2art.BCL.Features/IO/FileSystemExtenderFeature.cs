﻿namespace Base2art.IO
{
    using System;
    using System.IO;
    using System.Reflection;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class FileSystemExtenderFeature
    {
        [Test]
        public void ShouldValidateNulls()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            
            Action mut = () => FileSystemExtender.Dir(null, "abc");
            mut.ShouldThrow<ArgumentNullException>();
            
            mut = () => di.Dir(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
        
        [Test]
        public void ShouldGetParent()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.FullName.Should().BeEquivalentTo(di.Parent().FullName);
        }

        [Test]
        public void ShouldGetChildFile()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.File(di.Name).FullName.Should().BeEquivalentTo(di.FullName);
        }

        [Test]
        public void ShouldGetChildDir()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.Parent.Dir(di.Parent.Name).FullName.Should().BeEquivalentTo(di.Parent.FullName);
        }
    }
}
