﻿namespace Base2art.IO
{
    using System;
	using Base2art.Text;
	using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class EventedStringWriterFeature
    {
        [Test]
        public void ShouldProcessAutoFlush()
        {
            string text = string.Empty;
            using (var writer = new EventedStringWriter(true))
            {
                writer.Flushed += (sender, args) => { text += args.CurrentChunk; };
                text.Should().Be(string.Empty);
                writer.WriteLine("abc");
                text.Should().Be("abc\r\n");
                writer.WriteLine("123");
                text.Should().Be("abc\r\n123\r\n");
                writer.Write('1');
                text.Should().Be("abc\r\n123\r\n1");
                writer.Write("sdf".ToCharArray(), 1, 1);
                text.Should().Be("abc\r\n123\r\n1d");
                writer.Write("sdf");
                text.Should().Be("abc\r\n123\r\n1dsdf");
            }
        }
        
        [Test]
        public void ShouldProcessAutoFlushDefault()
        {
            string text = string.Empty;
            using (var writer = new EventedStringWriter())
            {
                writer.Flushed += (sender, args) => { text += args.CurrentChunk; };
                writer.Cleared += (sender, args) => { text = string.Empty; };
                text.Should().Be(string.Empty);
                writer.WriteLine("abc");
                text.Should().Be("abc\r\n");
                writer.WriteLine("123");
                text.Should().Be("abc\r\n123\r\n");
                writer.Write('1');
                text.Should().Be("abc\r\n123\r\n1");
                writer.Write("sdf".ToCharArray(), 1, 1);
                text.Should().Be("abc\r\n123\r\n1d");
                writer.Write("sdf");
                text.Should().Be("abc\r\n123\r\n1dsdf");
                writer.GetStringBuilder().ToString().Should().Be("abc\r\n123\r\n1dsdf");
                
                writer.Clear();
                writer.GetStringBuilder().ToString().Should().Be(string.Empty);
                text.Should().Be(string.Empty);
            }
        }
        
        [Test]
        public void ShouldProcessNoAutoFlush()
        {
            string text = string.Empty;
            using (var writer = new EventedStringWriter(false))
            {
                writer.Flushed += (sender, args) => { text += args.CurrentChunk; };
                text.Should().Be(string.Empty);
                writer.WriteLine("abc");
                text.Should().Be(string.Empty);
                writer.Flush();
                text.Should().Be("abc\r\n");
                writer.WriteLine("123");
                text.Should().Be("abc\r\n");
                writer.Flush();
                text.Should().Be("abc\r\n123\r\n");
            }
        }
    }
}
