﻿namespace Base2art.IO
{
    using System.IO;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class StreamExtenderFeature
    {
        [Test]
        public void ShouldReadFully()
        {
            var stream = new MemoryStream();
            stream.WriteByte((byte)243);
            stream.WriteByte((byte)222);
            stream.WriteByte((byte)12);
            stream.Seek(1L, SeekOrigin.Begin);
            
            var output = stream.ReadFully();
            output.Length.Should().Be(2);
            output[0].Should().Be(222);
            output[1].Should().Be(12);
        }
        
        [Test]
        public void ShouldReadFully_EmptyStream()
        {
            var stream = new MemoryStream();
            
            var output = stream.ReadFully();
            output.Length.Should().Be(0);
        }
        
    }
}
