﻿namespace Base2art
{
    using System;
    using Base2art.ComponentModel;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class NotNullFeature
    {
        [Test]
        public void ShouldCoalesce()
        {
            Person person = null;
            this.Coalesce1(person).Should().NotBeNull();
            this.Coalesce2(person).Should().NotBeNull();
            NotNull.Create<Person>(null).Value.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldGetValueWhenHasValue()
        {
            Person person = new Person();
            this.Coalesce1(person).Should().NotBeNull();
            this.Coalesce2(person).Should().NotBeNull();
            NotNull.Create<Person>(person).Value.Should().NotBeNull();
            object.ReferenceEquals(person, NotNull.Create<Person>(person).Value).Should().BeTrue();
        }
        
//        [Test]
//        public void ShouldCoalesceFromService()
//        {
//            var x = new ServiceLoaderInjector();
//            ServiceLoader.PrimaryLoader = x;
//            x.Bind<Person>()
//                .To(y=>new Person());
//            
//            Person person = null;
//            this.CoalesceFS1(person).Should().NotBeNull();
//            this.CoalesceFS2(person).Should().NotBeNull();
//            NotNull.Create<Person>(null).Value.Should().NotBeNull();
//            
//            ServiceLoader.PrimaryLoader = null;
//        }
        
//        [Test]
//        public void ShouldGetValueWhenHasValueFromService()
//        {
//            var x = new ServiceLoaderInjector();
//            ServiceLoader.PrimaryLoader = x;
//            x.Bind<Person>()
//                .To(y=>new Person());
//            
//            Person person = new Person();
//            this.CoalesceFS1(person).Should().NotBeNull();
//            this.CoalesceFS2(person).Should().NotBeNull();
//            NotNull.Create<Person>(person).Value.Should().NotBeNull();
//            object.ReferenceEquals(person, NotNull.Create<Person>(person).Value).Should().BeTrue();
//            
//            ServiceLoader.PrimaryLoader = null;
//        }
        
        [Test]
        public void ShouldVerifyEquality()
        {
            var a = new NotNull<Person>();
            var b = new NotNull<Person>();
            
            a.GetHashCode().Should().Be(0);
            b.GetHashCode().Should().Be(0);
            
            a.Equals(b).Should().BeTrue();
            a.Equals((object)b).Should().BeTrue();
            (a == b).Should().BeTrue();
            (a != b).Should().BeFalse();
            
            
            b.Equals(a).Should().BeTrue();
            b.Equals((object)a).Should().BeTrue();
            (b == a).Should().BeTrue();
            (b != a).Should().BeFalse();
        }
        
        
        [Test]
        public void ShouldVerifyEquality_SubEquals()
        {
            var a = new NotNull<Person>(new Person{Id = 1});
            var b = new NotNull<Person>(new Person{Id = 2});
            
            a.GetHashCode().Should().Be(1);
            b.GetHashCode().Should().Be(2);
            
            (a == b).Should().BeFalse();
            (a != b).Should().BeTrue();
        }
        
//        [Test]
//        public void ShouldVerifyEqualityForFromService()
//        {
//            var a = new NotNullFromService<Person>();
//            var b = new NotNullFromService<Person>();
//            
//            a.GetHashCode().Should().Be(0);
//            b.GetHashCode().Should().Be(0);
//            
//            a.Equals(b).Should().BeTrue();
//            a.Equals((object)b).Should().BeTrue();
//            (a == b).Should().BeTrue();
//            (a != b).Should().BeFalse();
//            
//            
//            b.Equals(a).Should().BeTrue();
//            b.Equals((object)a).Should().BeTrue();
//            (b == a).Should().BeTrue();
//            (b != a).Should().BeFalse();
//        }
        
        
//        [Test]
//        public void ShouldVerifyEquality_SubEqualsForFromService()
//        {
//            var a = new NotNullFromService<Person>(new Person{Id = 1});
//            var b = new NotNullFromService<Person>(new Person{Id = 2});
//            
//            a.GetHashCode().Should().Be(1);
//            b.GetHashCode().Should().Be(2);
//            
//            (a == b).Should().BeFalse();
//            (a != b).Should().BeTrue();
//        }

        private Person Coalesce1(NotNull<Person> p)
        {
            return p.Value;
        }

        private Person Coalesce2(NotNull<Person> p)
        {
            return p;
        }

//        private Person CoalesceFS1(NotNullFromService<Person> p)
//        {
//            return p.Value;
//        }

//        private Person CoalesceFS2(NotNullFromService<Person> p)
//        {
//            return p;
//        }
        
        private class Person
        {
            public int Id { get; set; }
            
            public override bool Equals(object obj)
            {
                Person other = obj as Person;
                if (other == null)
                {
                    return false;
                }
                
                return other.Id == this.Id;
            }
            
            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }
        }
    }
}


