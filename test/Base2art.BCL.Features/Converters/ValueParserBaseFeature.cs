﻿namespace Base2art.Converters
{
	using System.Net.Mail;
	using FluentAssertions;
	using NUnit.Framework;

	[TestFixture]
	public class ValueParserBaseFeature
	{
		[Test]
		public void ShouldParseOrDefaultSuccessfully()
		{
			var parser = new Int32Parser();
			var address = parser.ParseOrDefault("23");
			address.Should().Be(23);
		}

		[Test]
		public void ShouldParseOrDefaultUnsuccessfully()
		{
			var parser = new Int32Parser();
			var address = parser.ParseOrDefault("abc");
			address.Should().Be(0);
		}

		[Test]
		public void ShouldParseOrDefaultSuccessfullyWithSpecifiedDefault()
		{
			var parser = new Int32Parser();
			var address = parser.ParseOrDefault("23", 24);
			address.Should().Be(23);
		}

		[Test]
		public void ShouldParseOrDefaultUnsuccessfullyWithSpecifiedDefault()
		{
			var parser = new Int32Parser();
			var address = parser.ParseOrDefault("abc", 24);
			address.Should().Be(24);
		}

		[Test]
		public void ShouldParseOrDefaultUnsuccessfullyWithNullCreator()
		{
			var parser = new Int32Parser();
			var address = parser.ParseOrDefault("abc", (ICreator<int>)null);
			address.Should().Be(0);
		}

		[Test]
		public void ShouldParseUsingBase()
		{
			IObjectParserBase parser = new Int32Parser();
			parser.ParseOrDefault("abc").Should().Be(0);
			parser.ParseOrDefault("123").Should().Be(123);
		}
	}
}


