namespace Base2art.Converters
{
    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ByteParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            IValueParser<byte> parser = new ByteParser();
            parser.ParseOrNull(null).Should().Be(null);
            parser.ParseOrNull(string.Empty).Should().Be(null);
            parser.ParseOrNull("sdf").Should().Be(null);
            parser.ParseOrNull("012").Should().Be(12);
            parser.ParseOrNull("122").Should().Be(122);
            parser.ParseOrNull("0").Should().Be(0);
            parser.ParseOrNull("-1").Should().Be(null);

            parser.ParseOrDefault(null).Should().Be(0);
            parser.ParseOrDefault(string.Empty).Should().Be(0);
            parser.ParseOrDefault("sdf").Should().Be(0);
            parser.ParseOrDefault("012").Should().Be(12);
            parser.ParseOrDefault("122").Should().Be(122);
            parser.ParseOrDefault("0").Should().Be(0);
            parser.ParseOrDefault("-1").Should().Be(0);
        }
    }
}