namespace Base2art.Converters
{
    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class StringParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            IObjectParser<string> parser = new StringParser();
            parser.ParseOrDefault(null).Should().Be(null);
            parser.ParseOrDefault(string.Empty).Should().Be(string.Empty);
            parser.ParseOrDefault("ABC").Should().Be("ABC");


            parser.ParseOrDefault(null, "SOME VALUE").Should().Be("SOME VALUE");
            parser.ParseOrDefault(null, new FunctionalCreator<string>(() => "SOME VALUE")).Should().Be("SOME VALUE");
            parser.ParseOrDefault("ABC", new FunctionalCreator<string>(() => "SOME VALUE")).Should().Be("ABC");
        }
    }
}