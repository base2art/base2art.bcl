namespace Base2art.Converters
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class GuidParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            var value = Guid.NewGuid();
            IValueParser<Guid> parser = new GuidParser();
            parser.ParseOrNull(null).Should().Be(null);
            parser.ParseOrNull(string.Empty).Should().Be(null);
            parser.ParseOrNull("sdf").Should().Be(null);
            parser.ParseOrNull(value.ToString()).Should().Be(value);
            parser.ParseOrNull(Guid.Empty.ToString()).Should().Be(Guid.Empty);

            parser.ParseOrDefault(null).Should().Be(Guid.Empty);
            parser.ParseOrDefault(string.Empty).Should().Be(Guid.Empty);
            parser.ParseOrDefault("sdf").Should().Be(Guid.Empty);
            parser.ParseOrNull(value.ToString()).Should().Be(value);
            parser.ParseOrDefault(Guid.Empty.ToString()).Should().Be(Guid.Empty);
        }
    }
}