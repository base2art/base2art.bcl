﻿namespace Base2art.Converters
{
    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class SByteParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            IValueParser<sbyte> parser = new SByteParser();
            parser.ParseOrNull(null).Should().Be(null);
            parser.ParseOrNull(string.Empty).Should().Be(null);
            parser.ParseOrNull("sdf").Should().Be(null);
            parser.ParseOrNull("012").Should().Be((sbyte)12);
            parser.ParseOrNull("122").Should().Be((sbyte)122);
            parser.ParseOrNull("0").Should().Be((sbyte)0);
            parser.ParseOrNull("-1").Should().Be((sbyte)-1);

            parser.ParseOrDefault(null).Should().Be(0);
            parser.ParseOrDefault(string.Empty).Should().Be(0);
            parser.ParseOrDefault("sdf").Should().Be(0);
            parser.ParseOrDefault("012").Should().Be(12);
            parser.ParseOrDefault("122").Should().Be(122);
            parser.ParseOrDefault("0").Should().Be(0);
            parser.ParseOrDefault("-1").Should().Be(-1);
        }
    }
}