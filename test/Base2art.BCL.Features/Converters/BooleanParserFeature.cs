﻿namespace Base2art.Converters
{
    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class BooleanParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            IValueParser<bool> parser = new BooleanParser();
            parser.ParseOrNull(null).Should().Be(null);
            parser.ParseOrNull(string.Empty).Should().Be(null);
            parser.ParseOrNull("sdf").Should().Be(null);
            parser.ParseOrNull("true").Should().BeTrue();
            parser.ParseOrNull("TRUE").Should().BeTrue();
            parser.ParseOrNull("false").Should().BeFalse();
            parser.ParseOrNull("False").Should().BeFalse();


            parser.ParseOrDefault(null).Should().BeFalse();
            parser.ParseOrDefault(string.Empty).Should().BeFalse();
            parser.ParseOrDefault("sdf").Should().BeFalse();
            parser.ParseOrDefault("true").Should().BeTrue();
            parser.ParseOrDefault("TRUE").Should().BeTrue();
            parser.ParseOrDefault("false").Should().BeFalse();
            parser.ParseOrDefault("False").Should().BeFalse();

            parser.ParseOrDefault(null, true).Should().BeTrue();
            parser.ParseOrDefault(string.Empty, true).Should().BeTrue();
            parser.ParseOrDefault("sdf", true).Should().BeTrue();
            parser.ParseOrDefault("true", false).Should().BeTrue();
            parser.ParseOrDefault("TRUE", false).Should().BeTrue();
            parser.ParseOrDefault("false", true).Should().BeFalse();
            parser.ParseOrDefault("False", true).Should().BeFalse();

            parser.ParseOrDefault(null, new FunctionalCreator<bool>(() => true)).Should().BeTrue();
            parser.ParseOrDefault(string.Empty, new FunctionalCreator<bool>(() => true)).Should().BeTrue();
            parser.ParseOrDefault("sdf", new FunctionalCreator<bool>(() => true)).Should().BeTrue();
            parser.ParseOrDefault("true", new FunctionalCreator<bool>(() => false)).Should().BeTrue();
            parser.ParseOrDefault("TRUE", new FunctionalCreator<bool>(() => false)).Should().BeTrue();
            parser.ParseOrDefault("false",new FunctionalCreator<bool>( () => true)).Should().BeFalse();
            parser.ParseOrDefault("False", new FunctionalCreator<bool>(() => true)).Should().BeFalse();
        }
    }
}
