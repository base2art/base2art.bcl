﻿namespace Base2art.Converters
{
    using System.Net.Mail;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class ObjectParserBaseFeature
    {
        [Test]
        public void ShouldParseOrDefaultSuccessfully()
        {
            var parser = new MailAddressParser();
            var address =  parser.ParseOrDefault("scott.youngblut+test@gmail.com");
            address.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldParseOrDefaultUnsuccessfully()
        {
            var parser = new MailAddressParser();
            var address =  parser.ParseOrDefault("scott.youngb@lut+test@gmail.com");
            address.Should().BeNull();
        }
        
        [Test]
        public void ShouldParseOrDefaultSuccessfullyWithSpecifiedDefault()
        {
            var parser = new MailAddressParser();
            var address =  parser.ParseOrDefault("scott.youngblut+test@gmail.com", new MailAddress("scott.youngblut+test1@gmail.com"));
            address.Should().NotBeNull();
            address.Should().Be("scott.youngblut+test@gmail.com");
        }
        
        [Test]
        public void ShouldParseOrDefaultUnsuccessfullyWithSpecifiedDefault()
        {
            var parser = new MailAddressParser();
            var address =  parser.ParseOrDefault("scott@youngblut+test@gmail.com", new MailAddress("scott.youngblut+test1@gmail.com"));
            address.Should().NotBeNull();
            address.Should().Be("scott.youngblut+test1@gmail.com");
        }
        
        [Test]
        public void ShouldParseOrDefaultUnsuccessfullyWithNullCreator()
        {
            var parser = new MailAddressParser();
            var address =  parser.ParseOrDefault("scott@youngblut+test@gmail.com", (ICreator<MailAddress>)null);
            address.Should().BeNull();
        }
        
        [Test]
        public void ShouldParseUsingBase()
        {
            IObjectParserBase parser = new MailAddressParser();
            parser.ParseOrDefault("scott@youngblut+test@gmail.com").Should().BeNull();
            parser.ParseOrDefault("scott.youngblut+test@gmail.com").Should().NotBeNull();
        }
    }
    
    
}
