namespace Base2art.Converters
{
    using System.Net.Mail;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class MailAddressParserFeature
    {
        [Test]
        public void ShouldParse()
        {
            IObjectParser<MailAddress> parser = new MailAddressParser();
            parser.ParseOrDefault(null).Should().BeNull();
            parser.ParseOrDefault(string.Empty).Should().BeNull();
            parser.ParseOrDefault("ABC").Should().BeNull();

            parser.ParseOrDefault(null, new MailAddress("abc@base2art.com")).Should().Be(new MailAddress("abc@base2art.com"));
            parser.ParseOrDefault(null, new FunctionalCreator<MailAddress>(() => new MailAddress("abc@base2art.com"))).Should().Be(new MailAddress("abc@base2art.com"));

            parser.ParseOrDefault("sdfsdf@@@@", new MailAddress("abc@base2art.com")).Should().Be(new MailAddress("abc@base2art.com"));
            parser.ParseOrDefault("sdfsdf@@@@", new FunctionalCreator<MailAddress>(() => new MailAddress("abc@base2art.com"))).Should().Be(new MailAddress("abc@base2art.com"));
            
            parser.ParseOrDefault("abc@base2art.com",new FunctionalCreator<MailAddress>( () => new MailAddress("abc@base2art.com"))).Should().Be(new MailAddress("abc@base2art.com"));
        }
    }
}