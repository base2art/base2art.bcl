﻿namespace Base2art
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class OneTryEnsurerFeature
    {
        [Test]
        public void ShouldRetryTheEnsurer()
        {
            int i = -1;
            IEnsurer lazy = new OneTryEnsurer(() =>
            {
                i++;
                if (i < 3)
                {
                    throw new InvalidOperationException("Test");
                }
            });
            lazy.IsValueCreated.Should().BeFalse();
			new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeTrue();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Reset();
            lazy.IsValueCreated.Should().BeFalse();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeTrue();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Reset();
            i = 2;
            lazy.Ensure();
            i.Should().Be(3);
            lazy.IsValueCreated.Should().BeTrue();
        }
    }
}

