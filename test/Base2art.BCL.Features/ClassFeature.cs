﻿namespace Base2art
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ClassFeature
    {
        [Test]
        public void ShouldLoadByInstanceVal()
        {
            IClass<int> klazz = 3.GetClass();
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(int));
        }
        
        [Test]
        public void ShouldLoadByInstanceNullableVal()
        {
            int? i = 3;
            IClass<int?> klazz = i.GetClass();
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(int?));
        }

        [Test]
        public void ShouldLoadByInstanceRef()
        {
            IClass<string> klazz = "s".GetClass();
            IClass<object> oKlazz = klazz;
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(string));

            oKlazz.Should().NotBeNull();
            oKlazz.Type.Should().Be(typeof(string));
        }

        [Test]
        public void ShouldLoadByInstanceRefNull()
        {
            string s = null;
            IClass<string> klazz = s.GetClass();
            IClass<object> oKlazz = klazz;
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(string));

            oKlazz.Should().NotBeNull();
            oKlazz.Type.Should().Be(typeof(string));
        }

        [Test]
        public void ShouldLoadByTypeGeneric()
        {
            IClass<string> klazz = Class.GetClass<string>();
            IClass<object> oKlazz = klazz;
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(string));

            oKlazz.Should().NotBeNull();
            oKlazz.Type.Should().Be(typeof(string));
        }

        [Test]
        public void ShouldLoadByTypeRef()
        {
            var strType = typeof(Person);
            IClass klazz = strType.GetClass();
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(Person));

            this.CreateInstance(klazz.As<Person>()).Should().Be(new Person());
        }

        [Test]
        public void ShouldLoadByTypeRefBaseClass()
        {
            var strType = typeof(Employee);
            IClass klazz = strType.GetClass();
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(Employee));

            this.CreateInstance(klazz.As<Person>()).Should().BeOfType(typeof(Employee));
            this.CreateInstance(klazz.As<Employee>()).Should().BeOfType(typeof(Employee));
            (this.CreateInstance(klazz.As<Employee>()) is Employee).Should().BeTrue();
        }

        [Test]
        public void ShouldLoadByTypeVal()
        {
            var strType = typeof(Guid);
            IClass klazz = strType.GetClass();
            klazz.Should().NotBeNull();
            klazz.Type.Should().Be(typeof(Guid));

            this.CreateInstance(klazz.As<Guid>()).Should().Be(new Guid());
        }

        [Test]
        public void ShouldThrowErrorOnHack()
        {
            var strType = typeof(string);
            IClass klazz = strType.GetClass();
            klazz.Should().NotBeNull();
//            klazz.Type.Should().Be(typeof(Person));

            new Action(() => klazz.As<Person>()).ShouldThrow<System.InvalidCastException>();
        }

        private T CreateInstance<T>(IClass<T> klazz)
        {
            return (T)Activator.CreateInstance(klazz.Type);
        }

        private class Person : IEquatable<Person>
        {
            public override int GetHashCode()
            {
                return 1;
            }

            public override bool Equals(object obj)
            {
                return this.Equals((Person)obj);
            }

            public bool Equals(Person other)
            {
                return true;
            }
        }

        private class Employee : Person
        {
            public override int GetHashCode()
            {
                return 1;
            }

            public override bool Equals(object obj)
            {
                return this.Equals((Person)obj);
            }
        }
    }
}
