﻿namespace Base2art.ComponentModel
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class UsingFeature
    {
        private class SomeContainer 
        {
            public static String item;
        }
        
        [Test]
        public void ShouldAssignAndUnassign()
        {
            SomeContainer.item.Should().BeNull();
            using ("abc".Assign().TransactionallyTo(() => SomeContainer.item))
            {
                SomeContainer.item.Should().NotBeNull();
            }
            
            SomeContainer.item.Should().BeNull();
        }
        
        [Test]
        public void ShouldAssignAndUnassignOtherApi()
        {
            var loader = "abc";
            SomeContainer.item.Should().BeNull();
            using (Using.TransactionallyAssignTo(loader, () => SomeContainer.item))
            {
                SomeContainer.item.Should().NotBeNull();
                using (Using.TransactionallyAssignTo(null, () => SomeContainer.item))
                {
                    SomeContainer.item.Should().BeNull();
                }
                
                SomeContainer.item.Should().NotBeNull();
            }
            
            SomeContainer.item.Should().BeNull();
        }
        
        [Test]
        public void ShouldAssignAndUnassignOtherApiWithExceptions()
        {
            var loader = "ABC";
            SomeContainer.item.Should().BeNull();
            try
            {
                using (Using.TransactionallyAssignTo(loader, () => SomeContainer.item))
                {
                    SomeContainer.item.Should().NotBeNull();
                    try
                    {
                        using (Using.TransactionallyAssignTo(null, () => SomeContainer.item))
                        {
                            SomeContainer.item.Should().BeNull();
                            throw new Exception();
                        }
                    } catch (Exception) {
                        
                    }
                    
                    SomeContainer.item.Should().NotBeNull();
                    throw new Exception();
                }
            } catch (Exception) {
                
            }
            
            SomeContainer.item.Should().BeNull();
        }
    }
}
