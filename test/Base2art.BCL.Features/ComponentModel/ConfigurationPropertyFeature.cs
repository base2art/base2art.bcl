﻿// ReSharper disable once CheckNamespace
namespace Base2art.ComponentModel
{
    using Base2art.ComponentModel;
    using Base2art.Converters;
    using Base2art.IO;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ConfigurationPropertyFeature
    {
        [Test]
        public void ShouldGetAndSetValue()
        {
            var prop = new ConfigurationValueProperty<int>(new Int32Parser(), "SOMEVALUE");
            prop.Value = 83;
            prop.Value.Should().Be(83);
        }

        [Test]
        public void ShouldGetValueFromConfiguration()
        {
            var prop = new ConfigurationValueProperty<int>(new Int32Parser(), "sample.config.name1");

            prop.Value.Should().Be(1);
            prop.Value = (2);
            prop.Value.Should().Be(2);
        }

        [Test]
        public void ShouldGetValueFromConfigurationUniqueDefault()
        {
            var prop = new ConfigurationValueProperty<int>(new Int32Parser(), "sample.config.name2", 8);

            prop.Value.Should().Be(8);
            prop.Value = (2);
            prop.Value.Should().Be(2);
        }

        [Test]
        public void ShouldGetValueFromConfigurationUniqueDefaultNotDefined()
        {
            var prop = new ConfigurationValueProperty<int>(new Int32Parser(), "sample.config.name3", 8);

            prop.Value.Should().Be(8);
            prop.Value = (2);
            prop.Value.Should().Be(2);
        }

        [Test]
        public void ShouldGetValueFromConfigurationUniqueDefaultNotDefinedDelegate()
        {
            var prop = new ConfigurationValueProperty<int>(
                new Int32Parser(),
                "sample.config.name3",
                new FunctionalCreator<int>(() => 8 + 2));

            prop.Value.Should().Be(10);
            prop.Value = (2);
            prop.Value.Should().Be(2);
        }


        [Test]
        public void ShouldGetAndSetStringValue()
        {
            var prop = new ConfigurationObjectProperty<string>(new StringParser(), "SOMEVALUE");
            prop.Value = ("83");
            prop.Value.Should().Be("83");
        }

        [Test]
        public void ShouldGetStringValueFromConfiguration()
        {
            var prop = new ConfigurationObjectProperty<string>(new StringParser(), "sample.config.name1");

            prop.Value.Should().Be("1");
            prop.Value = ("2");
            prop.Value.Should().Be("2");
        }

        [Test]
        public void ShouldGetStringValueFromConfigurationUniqueDefault()
        {
            var prop = new ConfigurationObjectProperty<string>(new StringParser(), "sample.config.name2", "10");

            prop.Value.Should().Be("sdf");
            prop.Value = ("2");
            prop.Value.Should().Be("2");
        }

        [Test]
        public void ShouldGetStringValueFromConfigurationUniqueDefaultMissing()
        {
            var prop = new ConfigurationObjectProperty<string>(new StringParser(), "sample.config.name3", "10");

            prop.Value.Should().Be("10");
            prop.Value = ("2");
            prop.Value.Should().Be("2");
        }

        [Test]
        public void ShouldGetStringValueFromConfigurationUniqueDefaultMissingWithDelegate()
        {
            var prop = new ConfigurationObjectProperty<string>(
                new StringParser(),
                "sample.config.name3",
                new FunctionalCreator<string>(() => "10" + "sdf"));

            prop.Value.Should().Be("10sdf");
            prop.Value = ("2");
            prop.Value.Should().Be("2");
        }
    }
}
