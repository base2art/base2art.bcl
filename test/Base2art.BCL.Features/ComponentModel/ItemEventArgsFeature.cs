﻿namespace Base2art.ComponentModel
{    
    using System;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixtureAttribute]
    public class ItemEventArgsFeature
    {
        [Test]
        public void ShouldCreateArgumentsUsingInferment()
        {
            var item = ItemEventArgs.Create("Abc");
            item.Item.Should().Be("Abc");
        }
    }
}
