﻿namespace Base2art
{
    using System;
    using Base2art.ComponentModel;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class EventExtentionsFeature
    {
        [Test]
        public void ShouldInvokeNonGeneric()
        {
            var item = new EventHolder();
            var hasInvoked = false;
            item.Load += (sender, e) => hasInvoked = true;
            item.TriggerLoad();
            hasInvoked.Should().BeTrue();
        }
        
        [Test]
        public void ShouldInvokeNonGenericNotWired()
        {
            var item = new EventHolder();
            item.TriggerLoad();
        }
        
        [Test]
        public void ShouldInvokeStandardGeneric()
        {
            var item = new EventHolder();
            var hasInvoked = false;
            item.Click += (sender, e) => hasInvoked = true;
            item.TriggerClick();
            hasInvoked.Should().BeTrue();
        }
        
        [Test]
        public void ShouldInvokeStandardGenericNotWired()
        {
            var item = new EventHolder();
            item.TriggerClick();
        }
        
        [Test]
        public void ShouldInvokeCustomGeneric()
        {
            var item = new EventHolder();
            var hasInvoked = false;
            item.ItemChange += (sender, e) => hasInvoked = true;
            item.TriggerItemChange();
            hasInvoked.Should().BeTrue();
        }
        
        [Test]
        public void ShouldInvokeCustomGenericNotWired()
        {
            var item = new EventHolder();
            item.TriggerItemChange();
        }
        
        private class EventHolder
        {
            public event EventHandler<ItemEventArgs<string>> ItemChange;
            public event EventHandler<EventArgs> Click;
            public event EventHandler Load;

			public void TriggerClick()
			{
			    this.Click.InvokeSafely(this, EventArgs.Empty);
			}

			public void TriggerLoad()
			{
			    this.Load.InvokeSafely(this, EventArgs.Empty);
			}

			public void TriggerItemChange()
			{
			    this.ItemChange.InvokeSafely(this, string.Empty);
			}
        }
    }
}
