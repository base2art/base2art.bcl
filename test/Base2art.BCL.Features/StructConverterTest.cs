﻿namespace Base2art
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class StructConverterTest
    {
        [Test]
        public void ShouldConvertIntToStruct()
        {
            26.Struct().ConvertTo<Environment.SpecialFolder>()
                .Should().Be(Environment.SpecialFolder.ApplicationData);
        }
        
        [Test]
        public void ShouldConvertEnumToSameStruct()
        {
            Environment.SpecialFolder.ApplicationData.Struct().ConvertTo<Environment.SpecialFolder>()
                .Should().Be(Environment.SpecialFolder.ApplicationData);
        }
        
        [Test]
        public void ShouldConvertEnumToStruct()
        {
            AttributeTargets.Assembly.Struct().ConvertToInt32()
                .Should().Be(1);
            
            AttributeTargets.Assembly.Struct().ConvertTo<Base64FormattingOptions>()
                .Should().Be(Base64FormattingOptions.InsertLineBreaks);
        }
        
        [Test]
        public void ShouldConvertEnumToInt()
        {
            Environment.SpecialFolder.ApplicationData.Struct().ConvertToInt32()
                .Should().Be(26);
            
            
            ((IStruct)Environment.SpecialFolder.ApplicationData.Struct()).ConvertTo<int>()
                .Should().Be(26);
            
            ((IStruct)Environment.SpecialFolder.ApplicationData.Struct()).ConvertTo(typeof(int))
                .Should().Be(26);
        }
                
        [Test]
        public void ShouldConvertIntToInt()
        {
            26.Struct().ConvertToInt32()
                .Should().Be(26);
        }
        
        [Test]
        public void ShouldConvertLongToLong()
        {
            26L.Struct().ConvertToInt64()
                .Should().Be(26L);
        }
        
        [Test]
        public void ShouldConvertEnumToLong()
        {
            Environment.SpecialFolder.ApplicationData.Struct().ConvertToInt64()
                .Should().Be(26L);
        }
        
        [Test]
        public void ShouldConvertDataTypes()
        {
            ((byte)1).Struct().ConvertToByte().Should().Be(1);
            ((sbyte)1).Struct().ConvertToSByte().Should().Be(1);
            
            ((short)1).Struct().ConvertToInt16().Should().Be(1);
            ((ushort)1).Struct().ConvertToUInt16().Should().Be(1);
            
            ((int)1).Struct().ConvertToInt32().Should().Be(1);
            ((uint)1).Struct().ConvertToUInt32().Should().Be(1);
            
            ((long)1).Struct().ConvertToInt64().Should().Be(1);
            ((ulong)1).Struct().ConvertToUInt64().Should().Be(1);
        }
        
        [Test]
        public void ShouldConvertData()
        {
            1L.Should().Be(1);
            1L.Should().Be(1L);
            1.Struct().ConvertTo<long>().Should().Be(1L);
        }
    }
}
