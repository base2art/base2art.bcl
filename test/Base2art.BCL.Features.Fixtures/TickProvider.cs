﻿
namespace Base2art.BCL.Fixtures
{
    using System;

    public class TickProvider
    {
        public int TickCount
        {
            get
            {
                if (!this.Value.HasValue)
                {
                    return Environment.TickCount;
                }
                
                return this.Value.Value;
            }
        }
        
        public int? Value
        {
            get; set;
        }
    }
}